package br.americo.scp.util;

public interface Tags {

    //Telas do programa
    int TAG_TL_CADASTRO_SPLASH_SCREEN = 1;
    int TAG_TL_CADASTRO_LOGIN = 2;
    int TAG_TL_CADASTRO_USUARIO = 3;
    int TAG_TL_CADASTRO_PAUTA = 4;
    int TAG_TL_CONSULTA_PAUTA = 5;

    //Tags para usar com JSON
    String TAG_EMAIL = "email";
    String TAG_SENHA = "senha";
    String TAG_SHOW_LOGOUT = "showLogout";
    String TAG_PAUTA = "pauta";
}