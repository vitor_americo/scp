package br.americo.scp.util;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.CommandMap;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.MailcapCommandMap;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message.RecipientType;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendEmail extends Authenticator {

    private Multipart _multipart;

    private String host, port;
    private String login, password;
    private String subject, body;
    private String from;
    private boolean tsl;

    private List<String> lstTo;
    private List<String> lstCco;

    public SendEmail() {
        _multipart = new MimeMultipart();

        // There is something wrong with MailCap, javamail can not find a
        // handler for the multipart/mixed part, so this bit needs to be added.
        MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
        mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
        mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
        mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
        mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
        mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
        CommandMap.setDefaultCommandMap(mc);
    }

    public void send() throws Exception {
        Properties props = _setProperties();

        Session session = Session.getDefaultInstance(props, this);
        session.setDebug(true);

        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(from));

        if (lstTo != null) {
            for (String to : lstTo) {
                msg.addRecipient(RecipientType.TO, new InternetAddress(to));
            }
        }

        if (lstCco != null) {
            for (String cco : lstCco) {
                msg.addRecipient(RecipientType.BCC, new InternetAddress(cco));
            }
        }

        msg.setSubject(subject);
        msg.setSentDate(new Date());

        // setup message body
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText(body);
        _multipart.addBodyPart(messageBodyPart);

        // Put parts in message
        msg.setContent(_multipart);

        // send email
        Transport.send(msg);
    }

    public void addAttachment(String filename, String file) throws Exception {
        BodyPart messageBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(file);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(filename);

        _multipart.addBodyPart(messageBodyPart);
    }

    @Override
    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(login, password);
    }

    private Properties _setProperties() {
        Properties props = new Properties();

        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", host);
        props.put("mail.smtp.starttls.enable", String.valueOf(tsl));
        props.put("smtp.starttls.enable", String.valueOf(tsl));
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.socketFactory.port", port);

        if (host.equals("smtp.gmail.com"))
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

        return props;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setTsl(boolean tsl) {
        this.tsl = tsl;
    }

    public void setLstTo(List<String> lstTo) {
        this.lstTo = lstTo;
    }

    public void setLstCco(List<String> lstCco) {
        this.lstCco = lstCco;
    }
}