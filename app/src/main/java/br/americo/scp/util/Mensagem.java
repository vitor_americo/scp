package br.americo.scp.util;

import android.content.Context;
import android.graphics.Color;
import androidx.core.content.ContextCompat;

import com.muddzdev.styleabletoast.StyleableToast;

public class Mensagem {

    public static void toasty(Context context, String msg, int idColor) {
        new StyleableToast
                .Builder(context)
                .text(msg)
                .textColor(Color.WHITE)
                .backgroundColor(ContextCompat.getColor(context, idColor))
                .show();
    }
}