package br.americo.scp.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import java.util.concurrent.TimeUnit;

import androidx.appcompat.app.AppCompatActivity;
import br.americo.scp.R;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        openLogin();
    }

    private void openLogin() {
        new Handler().postDelayed(() -> {
            startActivity(new Intent(this, Login.class));
        }, TimeUnit.SECONDS.toMillis(3));
    }
}