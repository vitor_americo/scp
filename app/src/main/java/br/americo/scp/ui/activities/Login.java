package br.americo.scp.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;

import com.afollestad.materialdialogs.MaterialDialog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import br.americo.scp.R;
import br.americo.scp.databinding.FormLoginBinding;
import br.americo.scp.interfaces.CadastarEventListener;
import br.americo.scp.models.UsuarioModel;
import br.americo.scp.ui.pauta.PautaCon;
import br.americo.scp.ui.usuario.UsuarioCad;
import br.americo.scp.util.Mensagem;
import br.americo.scp.util.Tags;
import br.americo.scp.util.Util;
import br.americo.scp.viewModels.UsuarioViewModel;

public class Login extends AppCompatActivity implements OnClickListener {

    private UsuarioViewModel usuarioViewModel;

    private FormLoginBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.form_login);

        usuarioViewModel = ViewModelProviders.of(this).get(UsuarioViewModel.class);

        binding.setClickListener(this);

        onCreate();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabNovo:
                openUsuarioCad();
                break;
            case R.id.tvRecuperarSenha:
                dialogSendEmail();
                break;
            case R.id.btnEntrar:
                checkLogin();
                break;
            case R.id.btnCancelar:
                finish();
                break;
        }
    }

    private void onCreate() {
        binding.setUsuarioModel(new UsuarioModel());

        String email = Util.readPreference(this, Tags.TAG_EMAIL);
        String senha = Util.readPreference(this, Tags.TAG_SENHA);

        if (email != null && senha != null)
            login(email, senha);
    }

    private void checkLogin() {
        UsuarioModel usuarioModel = binding.getUsuarioModel();

        binding.edtEmail.setError(null);
        binding.edtSenha.setError(null);

        if (TextUtils.isEmpty(usuarioModel.getEmail())) {
            binding.edtEmail.setError(getString(R.string.campo_obrigatorio_em_branco));
            return;
        }

        if (TextUtils.isEmpty(usuarioModel.getSenha())) {
            binding.edtSenha.setError(getString(R.string.campo_obrigatorio_em_branco));
            return;
        }

        login(usuarioModel.getEmail(), usuarioModel.getSenha());
    }

    private void login(String email, String senha) {
        usuarioViewModel.get(email, senha, true).observe(this, usuarioModel -> {
            if (usuarioModel != null) {
                binding.setUsuarioModel(usuarioModel);
                setLogin(usuarioModel);
                openPautaCon();
            } else
                Mensagem.toasty(getApplicationContext(), getString(R.string.usuario_ou_senha_invalido), R.color.red);
        });
    }

    private void openUsuarioCad() {
        Intent intent = new Intent(this, UsuarioCad.class);
        intent.putExtra(Tags.TAG_SHOW_LOGOUT, false);
        startActivity(intent);
    }

    private void openPautaCon() {
        Intent intent = new Intent(this, PautaCon.class);
        startActivity(intent);
    }

    private void setLogin(UsuarioModel usuarioModel) {
        Util.addPreference(this, Tags.TAG_EMAIL, usuarioModel.getEmail());
        Util.addPreference(this, Tags.TAG_SENHA, usuarioModel.getSenha());
    }

    private void sendEmail(UsuarioModel usuarioModel) {
        usuarioViewModel.sendEmail(usuarioModel, new CadastarEventListener() {
            @Override
            public void onPreExecute() {
            }

            @Override
            public void onError(String error) {
                Mensagem.toasty(getApplicationContext(), error, R.color.red);
            }

            @Override
            public void result() {
                Mensagem.toasty(getApplicationContext(), getString(R.string.senha_enviada_com_sucesso), R.color.green);
            }
        });
    }

    private void dialogSendEmail() {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.recuperar_senha))
                .inputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
                .input(getString(R.string.digite_aqui_seu_email), "", false, (dialog, input) -> {
                    dialog.dismiss();

                    usuarioViewModel.get(input.toString(), true).observe(this, usuarioModel -> {
                        if (usuarioModel != null)
                            sendEmail(usuarioModel);
                        else {
                            Mensagem.toasty(getApplicationContext(), getString(R.string.email_invalido), R.color.red);
                            dialogSendEmail();
                        }
                    });
                })
                .cancelable(true)
                .positiveText(R.string.confirmar)
                .negativeText(R.string.cancelar)
                .show();
    }
}