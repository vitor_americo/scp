package br.americo.scp.ui.pauta;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import br.americo.scp.R;
import br.americo.scp.databinding.FormPautaCadBinding;
import br.americo.scp.interfaces.CadastarEventListener;
import br.americo.scp.interfaces.TextEventListener;
import br.americo.scp.models.PautaModel;
import br.americo.scp.util.Mensagem;
import br.americo.scp.util.Tags;
import br.americo.scp.util.Util;
import br.americo.scp.viewModels.PautaViewModel;
import br.americo.scp.viewModels.UsuarioViewModel;

public class PautaCad extends AppCompatActivity implements OnClickListener, TextEventListener {

    private PautaViewModel pautaViewModel;
    private UsuarioViewModel usuarioViewModel;

    private FormPautaCadBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.form_pauta_cad);

        pautaViewModel = ViewModelProviders.of(this).get(PautaViewModel.class);
        usuarioViewModel = ViewModelProviders.of(this).get(UsuarioViewModel.class);

        binding.setClickListener(this);
        binding.setTextEventListener(this);

        onCreate();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabSalvar:
                save(binding.getPautaModel());
                break;
        }
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        checkData();
    }

    private void onCreate() {
        PautaModel pautaModel = (PautaModel) getIntent().getSerializableExtra(Tags.TAG_PAUTA);

        if (pautaModel != null) {
            binding.setPautaModel(pautaModel);
        } else {
            binding.setPautaModel(new PautaModel());
            binding.fabSalvar.setEnabled(false);
            getUser();
        }
    }

    private void checkData() {
        if (TextUtils.isEmpty(binding.edtTitulo.getText().toString())
                || TextUtils.isEmpty(binding.edtDescricao.getText().toString())
                || TextUtils.isEmpty(binding.edtDetalhes.getText().toString())
                || TextUtils.isEmpty(binding.edtAutor.getText().toString())) {
            binding.fabSalvar.setEnabled(false);
        } else
            binding.fabSalvar.setEnabled(true);
    }

    private void save(PautaModel pautaModel) {
        pautaViewModel.save(pautaModel, new CadastarEventListener() {
            @Override
            public void onPreExecute() {
            }

            @Override
            public void onError(String error) {
                Mensagem.toasty(getApplicationContext(), error, R.color.red);
            }

            @Override
            public void result() {
                Mensagem.toasty(getApplicationContext(), getString(R.string.cadastro_realizado_com_sucesso), R.color.green);

                setResult(Activity.RESULT_OK);
                finish();
            }
        });
    }

    private void getUser() {
        String email = Util.readPreference(this, Tags.TAG_EMAIL);
        String senha = Util.readPreference(this, Tags.TAG_SENHA);

        if (email != null && senha != null) {
            usuarioViewModel.get(email, senha, true).observe(this, usuarioModel -> {
                if (usuarioModel != null)
                    binding.edtAutor.setText(usuarioModel.getNome());
            });
        }
    }
}