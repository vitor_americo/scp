package br.americo.scp.ui.pauta;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import br.americo.scp.R;
import br.americo.scp.adapters.PautaConAdapter;
import br.americo.scp.converters.StatusEnum;
import br.americo.scp.converters.StatusTypeConverter;
import br.americo.scp.databinding.FormPautaConBinding;
import br.americo.scp.interfaces.CadastarEventListener;
import br.americo.scp.models.PautaModel;
import br.americo.scp.ui.usuario.UsuarioCad;
import br.americo.scp.util.Mensagem;
import br.americo.scp.util.Tags;
import br.americo.scp.viewModels.PautaViewModel;

public class PautaCon extends AppCompatActivity implements OnClickListener, OnItemSelectedListener {

    private PautaConAdapter adpConsulta;

    private FormPautaConBinding binding;

    private PautaViewModel pautaViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.form_pauta_con);

        pautaViewModel = ViewModelProviders.of(this).get(PautaViewModel.class);

        binding.rvConsulta.setLayoutManager(new LinearLayoutManager(this));
        binding.rvConsulta.setHasFixedSize(true);

        binding.spnStatus.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, StatusEnum.values()));

        binding.setClickListener(this);
        binding.setItemSelectedListener(this);

        onCreate();
    }

    @Override
    public void onBackPressed() {
        this.finishAffinity();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mnu_pauta_con, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnu_perfil:
                openUsuarioCad();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabNovo:
                openPautaCad(null);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Tags.TAG_TL_CADASTRO_PAUTA:
                resultPautaCad(resultCode);
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        updateList(true);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    private void onCreate() {
        binding.edtFiltro.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adpConsulta != null)
                    adpConsulta.getFilter().filter(s.toString());
            }
        });

        binding.swrConsulta.setOnRefreshListener(() -> {
            if (adpConsulta != null) {
                updateList(false);
            } else
                binding.swrConsulta.setRefreshing(false);
        });

        createAdpter();
        updateList(true);
    }

    private void createAdpter() {
        adpConsulta = new PautaConAdapter((position, view) -> {
            PautaModel pautaModel = adpConsulta.getItem(position);

            switch (view.getId()) {
                case R.id.llPrincipal:
                    if (adpConsulta.getRowSelect() != position)
                        adpConsulta.selectRow(position);
                    else
                        adpConsulta.selectRow(-1);
                    break;
                case R.id.btnReabrir:
                    pautaModel.setStatus(StatusEnum.aberto);
                    save(pautaModel);
                    break;
                case R.id.btnFinalizar:
                    pautaModel.setStatus(StatusEnum.fechado);
                    save(pautaModel);
                    break;
                case R.id.llEditar:
                    openPautaCad(adpConsulta.getItem(position));
                    break;
                case R.id.llExcluir:
                    delete(adpConsulta.getItem(position));
                    break;
            }
        });

        binding.rvConsulta.setAdapter(adpConsulta);
    }

    private void updateList(boolean isSwipeRefresh) {
        if (isSwipeRefresh)
            binding.swrConsulta.setRefreshing(true);

        StatusEnum statusEnum = (StatusEnum) binding.spnStatus.getSelectedItem();

        pautaViewModel.getAll(StatusTypeConverter.toOrdinal(statusEnum)).observe(this, lstPautaModel -> {
            binding.swrConsulta.setRefreshing(false);

            if (lstPautaModel != null) {
                adpConsulta.setLstPautaModel(lstPautaModel);
                adpConsulta.notifyDataSetChanged();
                adpConsulta.getFilter().filter(binding.edtFiltro.getText().toString());
            }
        });
    }

    private void openUsuarioCad() {
        Intent intent = new Intent(this, UsuarioCad.class);
        intent.putExtra(Tags.TAG_SHOW_LOGOUT, true);
        startActivity(intent);
    }

    private void openPautaCad(PautaModel pautaModel) {
        Intent intent = new Intent(this, PautaCad.class);
        intent.putExtra(Tags.TAG_PAUTA, pautaModel);
        startActivityForResult(intent, Tags.TAG_TL_CADASTRO_PAUTA);
    }

    private void resultPautaCad(int resultCode) {
        if (resultCode == Activity.RESULT_OK)
            updateList(true);
    }

    private void save(PautaModel pautaModel) {
        pautaViewModel.save(pautaModel, new CadastarEventListener() {
            @Override
            public void onPreExecute() {
            }

            @Override
            public void onError(String error) {
                Mensagem.toasty(getApplicationContext(), error, R.color.red);
            }

            @Override
            public void result() {
                Mensagem.toasty(getApplicationContext(), getString(R.string.cadastro_realizado_com_sucesso), R.color.green);
                updateList(true);
            }
        });
    }

    private void delete(PautaModel pautaModel) {
        pautaViewModel.delete(pautaModel, new CadastarEventListener() {
            @Override
            public void onPreExecute() {
            }

            @Override
            public void onError(String error) {
                Mensagem.toasty(getApplicationContext(), error, R.color.red);
            }

            @Override
            public void result() {
                Mensagem.toasty(getApplicationContext(), getString(R.string.cadastro_realizado_com_sucesso), R.color.green);
                updateList(true);
            }
        });
    }
}