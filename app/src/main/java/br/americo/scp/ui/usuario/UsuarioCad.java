package br.americo.scp.ui.usuario;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import br.americo.scp.R;
import br.americo.scp.databinding.FormUsuarioCadBinding;
import br.americo.scp.interfaces.CadastarEventListener;
import br.americo.scp.models.UsuarioModel;
import br.americo.scp.util.Mensagem;
import br.americo.scp.util.Tags;
import br.americo.scp.util.Util;
import br.americo.scp.viewModels.UsuarioViewModel;

public class UsuarioCad extends AppCompatActivity implements OnClickListener {

    private UsuarioViewModel usuarioViewModel;

    private FormUsuarioCadBinding binding;

    private boolean showLogout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.form_usuario_cad);

        usuarioViewModel = ViewModelProviders.of(this).get(UsuarioViewModel.class);

        binding.setClickListener(this);

        onCreate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mnu_usuario_cad, menu);

        menu.getItem(0).setVisible(showLogout);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnu_logout:
                Util.addPreference(this, Tags.TAG_EMAIL, null);
                Util.addPreference(this, Tags.TAG_SENHA, null);
                this.finishAffinity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabSalvar:
                checkData();
                break;
        }
    }

    private void onCreate() {
        showLogout = getIntent().getBooleanExtra(Tags.TAG_SHOW_LOGOUT, false);

        String email = Util.readPreference(this, Tags.TAG_EMAIL);
        String senha = Util.readPreference(this, Tags.TAG_SENHA);

        if (email != null && senha != null) {
            usuarioViewModel.get(email, senha, true).observe(this, usuarioModel -> {
                if (usuarioModel != null)
                    binding.setUsuarioModel(usuarioModel);
                else
                    binding.setUsuarioModel(new UsuarioModel());
            });
        } else
            binding.setUsuarioModel(new UsuarioModel());
    }

    private void checkData() {
        UsuarioModel usuarioModel = binding.getUsuarioModel();

        binding.edtNome.setError(null);
        binding.edtEmail.setError(null);
        binding.edtSenha.setError(null);
        binding.edtSenhaConfirmar.setError(null);

        if (TextUtils.isEmpty(usuarioModel.getNome())) {
            binding.edtNome.setError(getString(R.string.campo_obrigatorio_em_branco));
            return;
        }

        if (TextUtils.isEmpty(usuarioModel.getEmail())) {
            binding.edtEmail.setError(getString(R.string.campo_obrigatorio_em_branco));
            return;
        }

        if (TextUtils.isEmpty(usuarioModel.getSenha())) {
            binding.edtSenha.setError(getString(R.string.campo_obrigatorio_em_branco));
            return;
        }

        if (TextUtils.isEmpty(binding.edtSenhaConfirmar.getText().toString())) {
            binding.edtSenhaConfirmar.setError(getString(R.string.campo_obrigatorio_em_branco));
            return;
        }

        if (!usuarioModel.getSenha().equals(binding.edtSenhaConfirmar.getText().toString())) {
            binding.edtSenhaConfirmar.setError(getString(R.string.senha_nao_confere));
            return;
        }

        save(usuarioModel);
    }

    private void setLogin(UsuarioModel usuarioModel) {
        Util.addPreference(this, Tags.TAG_EMAIL, usuarioModel.getEmail());
        Util.addPreference(this, Tags.TAG_SENHA, usuarioModel.getSenha());
    }

    private void save(UsuarioModel usuarioModel) {
        usuarioViewModel.save(usuarioModel, new CadastarEventListener() {
            @Override
            public void onPreExecute() {
            }

            @Override
            public void onError(String error) {
                Mensagem.toasty(getApplicationContext(), error, R.color.red);
            }

            @Override
            public void result() {
                if (showLogout)
                    setLogin(usuarioModel);

                Mensagem.toasty(getApplicationContext(), getString(R.string.cadastro_realizado_com_sucesso), R.color.green);

                finish();
            }
        });
    }
}