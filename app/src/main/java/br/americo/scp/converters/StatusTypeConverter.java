package br.americo.scp.converters;

import androidx.room.TypeConverter;

public class StatusTypeConverter {
    @TypeConverter
    public static StatusEnum toStatus(int ordinal) {
        return StatusEnum.values()[ordinal];
    }

    @TypeConverter
    public static Integer toOrdinal(StatusEnum value) {
        return value.ordinal();
    }
}