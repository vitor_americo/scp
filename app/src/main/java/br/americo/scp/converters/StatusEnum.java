package br.americo.scp.converters;

public enum StatusEnum {
    aberto("Aberto"),
    fechado("Fechado");

    private String display;

    StatusEnum(String display) {
        this.display = display;
    }

    @Override
    public String toString() {
        return display;
    }
}