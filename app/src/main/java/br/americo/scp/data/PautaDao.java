package br.americo.scp.data;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import br.americo.scp.converters.StatusEnum;
import br.americo.scp.models.PautaModel;

@Dao
public interface PautaDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(PautaModel model);

    @Update
    void update(PautaModel... model);

    @Delete
    void delete(PautaModel... model);

    @Query("SELECT * FROM pauta WHERE codigo = :codigo")
    PautaModel get(int codigo);

    @Query("SELECT * FROM pauta WHERE status = :statusEnum")
    List<PautaModel> getAll(int statusEnum);
}