package br.americo.scp.data;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import br.americo.scp.models.UsuarioModel;

@Dao
public interface UsuarioDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(UsuarioModel model);

    @Update
    void update(UsuarioModel... model);

    @Delete
    void delete(UsuarioModel... model);

    @Query("SELECT * FROM usuario WHERE email = :email")
    UsuarioModel get(String email);

    @Query("SELECT * FROM usuario WHERE email = :email AND senha = :senha")
    UsuarioModel get(String email, String senha);

    @Query("SELECT * FROM usuario")
    List<UsuarioModel> getAll();
}