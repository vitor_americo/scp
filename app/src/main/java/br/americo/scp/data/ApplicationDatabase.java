package br.americo.scp.data;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import br.americo.scp.models.PautaModel;
import br.americo.scp.models.UsuarioModel;

@Database(entities = {
        UsuarioModel.class,
        PautaModel.class},
        version = 1,
        exportSchema = false)
public abstract class ApplicationDatabase extends RoomDatabase {

    private static ApplicationDatabase applicationDatabaseInstance;
    private static String DATABASE_NAME = "scp.db";

    public static ApplicationDatabase getDatabase(Context context) {
        if (applicationDatabaseInstance == null) {
            applicationDatabaseInstance = Room.databaseBuilder(context.getApplicationContext(), ApplicationDatabase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration().build();
        }

        return applicationDatabaseInstance;
    }

    public abstract UsuarioDao usuarioDao();

    public abstract PautaDao pautaDao();
}