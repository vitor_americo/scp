package br.americo.scp.interfaces;

public interface TextEventListener {

    void onTextChanged(CharSequence s, int start, int before, int count);
}