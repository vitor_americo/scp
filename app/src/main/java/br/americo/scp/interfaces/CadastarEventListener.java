package br.americo.scp.interfaces;

public interface CadastarEventListener {

    void onPreExecute();

    void onError(String error);

    void result();
}