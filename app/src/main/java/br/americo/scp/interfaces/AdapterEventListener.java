package br.americo.scp.interfaces;

import android.view.View;

public interface AdapterEventListener {

    void onItemClick(int position, View view);
}