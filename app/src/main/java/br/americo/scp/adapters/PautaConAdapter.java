package br.americo.scp.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import br.americo.scp.R;
import br.americo.scp.databinding.ItensPautaConBinding;
import br.americo.scp.interfaces.AdapterEventListener;
import br.americo.scp.models.PautaModel;

public class PautaConAdapter extends RecyclerSwipeAdapter<PautaConAdapter.PautaViewHolder> implements Filterable {

    private List<PautaModel> mOriginalValues = new ArrayList<>(); // Original Values
    private List<PautaModel> mDisplayedValues = new ArrayList<>();// Values to be displayed

    private LayoutInflater layoutInflater;

    private AdapterEventListener listener;

    private int rowSelect = -1;

    public PautaConAdapter(AdapterEventListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public PautaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.getContext());

        ItensPautaConBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.itens_pauta_con, parent, false);
        binding.setAdapterEventListener(listener);

        return new PautaViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final PautaViewHolder holder, int position) {
        final PautaModel pautaModel = getItem(position);

        holder.binding.setPautaModel(pautaModel);
        holder.binding.setIndex(position);
        holder.binding.setRowSelect(rowSelect);
        holder.binding.executePendingBindings();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mDisplayedValues = (ArrayList<PautaModel>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<PautaModel> FilteredArrList = new ArrayList<>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {
                    // set the Original result to return
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toUpperCase();
                    for (int i = 0; i < mOriginalValues.size(); i++) {
                        if (checkFilter(constraint.toString(), mOriginalValues.get(i))) {
                            FilteredArrList.add(mOriginalValues.get(i));
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeLayout;
    }

    @Override
    public int getItemCount() {
        return mDisplayedValues != null ? mDisplayedValues.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return mDisplayedValues != null ? mDisplayedValues.get(position).getCodigo() : 0;
    }

    public PautaModel getItem(int position) {
        return mDisplayedValues.get(position);
    }

    public int getRowSelect() {
        return rowSelect;
    }

    public void setLstPautaModel(List<PautaModel> lstPautaModel) {
        this.mOriginalValues = lstPautaModel;
        this.mDisplayedValues = lstPautaModel;
    }

    public void selectRow(int newRowSelect) {
        int oldRowSelect = rowSelect;
        rowSelect = newRowSelect;

        notifyItemChanged(newRowSelect);

        if (oldRowSelect >= 0)
            notifyItemChanged(oldRowSelect);
    }

    private boolean checkFilter(String filtro, PautaModel pautaModel) {
        //Filtro titulo/descrição
        if (!pautaModel.getTitulo().toUpperCase().contains(filtro)
                && !pautaModel.getDescricao().toUpperCase().contains(filtro))
            return false;

        return true;
    }

    class PautaViewHolder extends RecyclerView.ViewHolder {

        private final ItensPautaConBinding binding;

        PautaViewHolder(final ItensPautaConBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }
}