package br.americo.scp.models;

import java.io.Serializable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import br.americo.scp.converters.StatusEnum;
import br.americo.scp.converters.StatusTypeConverter;

@Entity(tableName = "pauta")
public class PautaModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int codigo;
    private String titulo;
    private String descricao;
    private String detalhes;
    private String autor;
    @TypeConverters(StatusTypeConverter.class)
    private StatusEnum status = StatusEnum.aberto;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDetalhes() {
        return detalhes;
    }

    public void setDetalhes(String detalhes) {
        this.detalhes = detalhes;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }
}