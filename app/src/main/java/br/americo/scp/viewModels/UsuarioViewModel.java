package br.americo.scp.viewModels;

import android.app.Application;
import android.os.AsyncTask;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import br.americo.scp.data.ApplicationDatabase;
import br.americo.scp.interfaces.CadastarEventListener;
import br.americo.scp.models.UsuarioModel;
import br.americo.scp.util.SendEmail;

public class UsuarioViewModel extends AndroidViewModel {

    private MutableLiveData<UsuarioModel> mdUsuarioModel;

    private ApplicationDatabase applicationDatabase;

    private ExecutorService executorService;

    public UsuarioViewModel(@NonNull Application application) {
        super(application);

        applicationDatabase = ApplicationDatabase.getDatabase(this.getApplication());

        executorService = Executors.newSingleThreadExecutor();
    }

    public void save(UsuarioModel usuarioModel, CadastarEventListener listener) {
        new SaveAsync(applicationDatabase, listener).execute(usuarioModel);
    }

    public LiveData<UsuarioModel> get(String email, String senha, boolean refresh) {
        if (mdUsuarioModel == null || refresh) {
            mdUsuarioModel = new MutableLiveData<>();

            executorService.execute(() ->
                    mdUsuarioModel.postValue(applicationDatabase.usuarioDao().get(email, senha)));
        }

        return mdUsuarioModel;
    }

    public LiveData<UsuarioModel> get(String email, boolean refresh) {
        if (mdUsuarioModel == null || refresh) {
            mdUsuarioModel = new MutableLiveData<>();

            executorService.execute(() ->
                    mdUsuarioModel.postValue(applicationDatabase.usuarioDao().get(email)));
        }

        return mdUsuarioModel;
    }

    public void sendEmail(UsuarioModel usuarioModel, CadastarEventListener listener) {
        new SendEmailAsync(listener).execute(usuarioModel);
    }

    private static class SaveAsync extends AsyncTask<UsuarioModel, String, String> {

        private ApplicationDatabase applicationDatabase;

        private final CadastarEventListener listener;

        SaveAsync(ApplicationDatabase applicationDatabase, CadastarEventListener listener) {
            this.applicationDatabase = applicationDatabase;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (listener != null)
                listener.onPreExecute();
        }

        @Override
        protected String doInBackground(final UsuarioModel... params) {
            try {
                applicationDatabase.usuarioDao().insert(params[0]);
            } catch (Exception e) {
                return " " + e.getMessage();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String error) {
            if (listener != null) {
                if (TextUtils.isEmpty(error))
                    listener.result();
                else
                    listener.onError(error);
            }
        }
    }

    private static class SendEmailAsync extends AsyncTask<UsuarioModel, String, String> {

        private final CadastarEventListener listener;

        SendEmailAsync(CadastarEventListener listener) {
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (listener != null)
                listener.onPreExecute();
        }

        @Override
        protected String doInBackground(final UsuarioModel... params) {
            try {
                UsuarioModel usuarioModel = params[0];

                SendEmail sendEmail = new SendEmail();
                sendEmail.setHost("");//host smtp
                sendEmail.setPort("");//porta
                sendEmail.setLogin("");//Email login
                sendEmail.setPassword("");//Senha do email
                sendEmail.setTsl(true);//Segurança

                sendEmail.setSubject("Recuperar Senha");
                sendEmail.setBody("Sua senha do app SCP é " + usuarioModel.getSenha());
                sendEmail.setFrom("");//Email envio

                List<String> lstEmailTo = new ArrayList<>();
                lstEmailTo.add(usuarioModel.getEmail());
                sendEmail.setLstTo(lstEmailTo);

                sendEmail.send();
            } catch (Exception e) {
                return " " + e.getMessage();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String error) {
            if (listener != null) {
                if (TextUtils.isEmpty(error))
                    listener.result();
                else
                    listener.onError(error);
            }
        }
    }
}