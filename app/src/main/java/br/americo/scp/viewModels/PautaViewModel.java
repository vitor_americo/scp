package br.americo.scp.viewModels;

import android.app.Application;
import android.os.AsyncTask;
import android.text.TextUtils;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import br.americo.scp.data.ApplicationDatabase;
import br.americo.scp.interfaces.CadastarEventListener;
import br.americo.scp.models.PautaModel;

public class PautaViewModel extends AndroidViewModel {

    private MutableLiveData<List<PautaModel>> mdLstPautaModel;

    private ApplicationDatabase applicationDatabase;

    private ExecutorService executorService;

    public PautaViewModel(@NonNull Application application) {
        super(application);

        applicationDatabase = ApplicationDatabase.getDatabase(this.getApplication());

        executorService = Executors.newSingleThreadExecutor();
    }

    public void save(PautaModel pautaModel, CadastarEventListener listener) {
        new SaveAsync(applicationDatabase, listener).execute(pautaModel);
    }

    public void delete(PautaModel pautaModel, CadastarEventListener listener) {
        new DeleteAsync(applicationDatabase, listener).execute(pautaModel);
    }

    public LiveData<List<PautaModel>> getAll(int statusEnum) {
        mdLstPautaModel = new MutableLiveData<>();

        executorService.execute(() ->
                mdLstPautaModel.postValue(applicationDatabase.pautaDao().getAll(statusEnum)));

        return mdLstPautaModel;
    }

    private static class SaveAsync extends AsyncTask<PautaModel, String, String> {

        private ApplicationDatabase applicationDatabase;

        private final CadastarEventListener listener;

        SaveAsync(ApplicationDatabase applicationDatabase, CadastarEventListener listener) {
            this.applicationDatabase = applicationDatabase;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (listener != null)
                listener.onPreExecute();
        }

        @Override
        protected String doInBackground(final PautaModel... params) {
            try {
                applicationDatabase.pautaDao().insert(params[0]);
            } catch (Exception e) {
                return " " + e.getMessage();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String error) {
            if (listener != null) {
                if (TextUtils.isEmpty(error))
                    listener.result();
                else
                    listener.onError(error);
            }
        }
    }

    private static class DeleteAsync extends AsyncTask<PautaModel, String, String> {

        private ApplicationDatabase applicationDatabase;

        private final CadastarEventListener listener;

        DeleteAsync(ApplicationDatabase applicationDatabase, CadastarEventListener listener) {
            this.applicationDatabase = applicationDatabase;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (listener != null)
                listener.onPreExecute();
        }

        @Override
        protected String doInBackground(final PautaModel... params) {
            try {
                applicationDatabase.pautaDao().delete(params[0]);
            } catch (Exception e) {
                return " " + e.getMessage();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String error) {
            if (listener != null) {
                if (TextUtils.isEmpty(error))
                    listener.result();
                else
                    listener.onError(error);
            }
        }
    }
}